<?php

use yii\db\Migration;

class m151029_130415_users extends Migration
{
    public function up()
    {
        $options = 'ENGINE=INNODB DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci';
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'password' => $this->string(255)->notNull(),
            'authKey' => $this->string(255),
            'accessToken' => $this->string(255),
            'role' => $this->smallInteger(1),
            'created' => $this->timestamp()->notNull() . " DEFAULT CURRENT_TIMESTAMP"
        ], $options);
    }

    public function down()
    {
        echo "m151029_130415_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
