<?php

use yii\db\Migration;

class m151029_135247_users_board extends Migration
{
    public function up()
    {
        $options = 'ENGINE=INNODB DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci';
        $this->createTable('users_board', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(255)->notNull(),
            'patronymic' => $this->string(255)->notNull(),
            'last_name' => $this->string(255)->notNull(),
            'about' => $this->text()->notNull(),
            'created' => $this->timestamp()->notNull() . " DEFAULT CURRENT_TIMESTAMP"
        ], $options);
    }

    public function down()
    {
        echo "m151029_135247_users_board cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
