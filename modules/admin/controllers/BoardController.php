<?php

namespace app\modules\admin\controllers;

use app\models\UsersBoard;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class BoardController extends Controller
{
    public function actionIndex()
    {
        $models = UsersBoard::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $models,
            'pagination' => ['pageSize' => 20],
            'sort' => ['defaultOrder' => ['created' => SORT_DESC]]
        ]);

        return $this->render('index', compact('dataProvider'));
    }

    public function actionAdd()
    {
        $model = new UsersBoard();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            return $this->redirect('/admin/board/add');
        }
        return $this->render('add', [
            'model' => $model,
        ]);
    }

    public function actionEdit($id)
    {
        $model = UsersBoard::findOne(['id' => intval($id)]);

        if (empty($model)) {
            throw new NotFoundHttpException();
        }

        if (Yii::$app->request->post()) {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    return $this->redirect(['/admin/board']);
                }
            }
        }
        return $this->render('edit', compact('model'));
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}