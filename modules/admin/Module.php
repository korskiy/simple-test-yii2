<?php

namespace app\modules\admin;

use Yii;

class Module extends \app\Module
{
    public $controllerNamespace = 'app\modules\admin\controllers';
}