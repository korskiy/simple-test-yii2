<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = 'Состав наблюдательного совета';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-default-index">
    <div class="row">
        <div class="col-lg-9">

            <h1><?= $this->title ?></h1>
            <div class="form-group">
                <?= Html::a('Добавить', '/admin/board/add', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9">
            <?php if ($dataProvider->getTotalCount() > 0): ?>
                <table class="table table-bordered table-striped">
                    <tbody>
                    <?php foreach($dataProvider->getModels() as $userBoard): ?>
                        <tr>
                            <td>
                                <?= Html::encode($userBoard->first_name) ?>
                            </td>
                            <td>
                                <?= Html::encode($userBoard->last_name) ?>
                            </td>
                            <td>
                                <?= Html::encode($userBoard->patronymic) ?>
                            </td>
                            <td>
                                <?= Html::a('Редактировать', ['/admin/board/' . $userBoard->id . '/edit']) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-lg-12">
                        <?= \yii\widgets\LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
                    </div>
                </div>
            <?php else: ?>
                <p>
                    Список пуст.
                </p>
            <?php endif; ?>
        </div>
    </div>
</div>