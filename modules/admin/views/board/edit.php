<?php


$this->title = 'Редактирование сотрудника';
$this->params['breadcrumbs'][] = ['label' => 'Состав наблюдательного совета', 'url' => ['/admin/board']];;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="row">
        <div class="col-md-6" >
            <h1><?= $this->title ?></h1>
            <?= $this->render('_form', ['model' => $model]) ?>
        </div>
    </div>
</div>