<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'enableClientValidation' => false,
]); ?>
<?= $form->field($model, 'first_name') ?>
<?= $form->field($model, 'patronymic') ?>
<?= $form->field($model, 'last_name') ?>
<?= $form->field($model, 'about')->textarea(['rows' => 8]) ?>
    <div class="form-group">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>