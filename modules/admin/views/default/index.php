<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = 'Панель администратора';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-default-index">
    <div class="row">
        <div class="col-lg-9">
            <?= Html::a('Состав наблюдательного совета', '/admin/board') ?>

        </div>
    </div>
</div>