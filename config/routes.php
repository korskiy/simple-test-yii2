<?php
return [
    '' => 'site/index',
    '/admin/board/<id:\d+>/edit' => '/admin/board/edit',
    'site/login' => 'site/login',
    'admin' => 'admin/default/index',
    'default/index' => 'admin/board/add',
    //'login' => 'site/login',
    //'logout' => 'site/logout',
];