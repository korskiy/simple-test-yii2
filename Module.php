<?php

namespace app;

use Yii;

abstract class Module extends \yii\base\Module
{
    public $modelClasses = [];
    protected $_models;


    public function init()
    {
        parent::init();
        $this->modelClasses = array_merge($this->_getDefaultModelClasses(), $this->modelClasses);
    }

    protected function _getDefaultModelClasses() {
        return [];
    }

    /**
     * Get object instance of model
     * @param string $name
     * @param array $config
     * @return ActiveRecord
     */
    public function model($name, $config = []) {

        // return object if already created
        if (!empty($this->_models[$name])) {
            return $this->_models[$name];
        }

        // create object
        $className = $this->modelClasses[ucfirst($name)];
        $this->_models[$name] = Yii::createObject(array_merge(["class" => $className], $config));

        return $this->_models[$name];
    }
}