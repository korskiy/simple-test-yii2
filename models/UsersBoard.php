<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users_board".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $patronymic
 * @property string $last_name
 * @property string $about
 * @property string $created
 */
class UsersBoard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_board';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'patronymic', 'last_name'], 'required'],
            [['about'], 'string'],
            [['created'], 'safe'],
            [['first_name', 'patronymic', 'last_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'Имя',
            'patronymic' => 'Отчество',
            'last_name' => 'Фамилия',
            'about' => 'Описание',
            'created' => 'Дата добавления',
        ];
    }
}
